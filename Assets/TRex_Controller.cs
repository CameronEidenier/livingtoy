﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class TRex_Controller : MonoBehaviour
{
    Animator anim;
    AudioSource roarSound;
    public GameObject targetTRex;
    public GameObject targetBox;
    Vector3 rotationLast;
    SkinnedMeshRenderer render;
    public bool active;
    float speed = .75f;
    Vector3 rotationPre;
    Quaternion originalRotation;
    Quaternion newLocalRotation;
    Quaternion newWorldRotation;
    float prevAngle = 0;
    bool turning = false;
    bool roaring = false;
    public bool attacking = false;
    // Use this for initialization
    void Start()
    {
        render = GetComponentInChildren<SkinnedMeshRenderer>();
        anim = GetComponent<Animator>();
        roarSound = GetComponent<AudioSource>();
        //if (tRex_A) targetTRex = GameObject.FindGameObjectWithTag("T-Rex_B");
        //else targetTRex = GameObject.FindGameObjectWithTag("T-Rex_A");
    }

    float angleChange;
    Quaternion lastQuat = new Quaternion();
    // Update is called once per frame
    void Update()
    {

        if (render.enabled) active = true;
        else active = false;


        //print(Mathf.Abs(prevAngle) - Mathf.Abs(transform.localRotation.y));
        //print("Prev:" + prevAngle + " Current:" + transform.localRotation.y);
        prevAngle = transform.localRotation.y;
        float steps = speed * Time.deltaTime; 
        targetBox.transform.position = targetTRex.transform.position;
        if (targetTRex.GetComponent<Stegosaurus_Controller>().active)
        {
            float angletTarget = angleBetween2CartesianPoints(transform.localPosition.x, transform.localPosition.z, targetBox.transform.localPosition.x, targetBox.transform.localPosition.z);
            if (!roaring) transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.Euler(0f, angletTarget, 0f), steps);
            if (Mathf.Abs(transform.localRotation.eulerAngles.y - angletTarget) > 2f) turning = true;
            else turning = false;

            attacking = (Vector3.Distance(transform.localPosition, targetBox.transform.localPosition) < 2f && !turning);
        }
        else turning = false;
        
        //else attacking = false;
        if (roaring)
        {
            turning = false;
            attacking = false;
        }
        anim.SetBool("Turning", turning);
        anim.SetBool("Roar", roaring);
        anim.SetBool("Attack", attacking);
    }
    public float angleBetween2CartesianPoints(float firstX, float firstY, float secondX, float secondY)
    {
        float angle = Mathf.Atan2((secondX - firstX), (secondY - firstY)) * 180 / Mathf.PI;
        if (angle < 0)
        {
            return (360 + angle);
        }
        else
        {
            return (angle);
        }
    }
    public void Roar()
    {
            roaring = true;
    }
    public void RoarSound()
    {
        roarSound.Play();
    }
    public void EndRoar()
    {
        roaring = false;
    }
}
