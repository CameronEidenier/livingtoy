Shader "YETi/VR_VegetationLeaves" {
    Properties {
        _Color ("Base Color", Color) = (1,1,1,1)
        _Diffuse ("Diffuse", 2D) = "white" {}
		_AlphaCutoff ("Alpha Cutoff", Range(0,1)) = 0.5
		_AmbientColor ("Ambient Color", Color) = (0.05, 0.05, 0.05, 1)
        _SpecularColor ("Specular Color", Color) = (1.0, 1.0, 1.0, 1)
		_Gloss ("Gloss", Range (0.01, 1.0)) = 0.078125
		_LightWrap ("Light Wrap (Half Lambert)", Range (0.0, 1.0)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Off

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
			#define SUPPORT_GI	// Uncomment to compile with support for Unity GI
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            //#pragma exclude_renderers d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _Color;
            uniform sampler2D _Diffuse; uniform float4 _Diffuse_ST;
            uniform float4 _SpecularColor;
            uniform float _Gloss;
			uniform float4 _AmbientColor;
            uniform float _AlphaCutoff;
			uniform float _LightWrap;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                //float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                //float3 tangentDir : TEXCOORD5;
                //float3 bitangentDir : TEXCOORD6;
                LIGHTING_COORDS(7,8)
                UNITY_FOG_COORDS(9)
                #if defined(LIGHTMAP_ON) || defined(UNITY_SHOULD_SAMPLE_SH)
                    float4 ambientOrLightmapUV : TEXCOORD10;
                #endif
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
				o.uv0 = TRANSFORM_TEX(v.texcoord0, _Diffuse);
                #ifdef LIGHTMAP_ON
                    o.ambientOrLightmapUV.xy = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
                    o.ambientOrLightmapUV.zw = 0;
                #endif
                #ifdef DYNAMICLIGHTMAP_ON
                    o.ambientOrLightmapUV.zw = v.texcoord2.xy * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
                #endif
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                //o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                //o.bitangentDir = normalize( cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float4 _Diffuse_var = tex2D(_Diffuse, i.uv0);
                clip(_Diffuse_var.a - _AlphaCutoff);
				
				float faceSign = ( facing >= 0 ? 1 : -1 );
				float3 normalDirection = normalize( i.normalDir );
                normalDirection *= faceSign;

                //float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, normalDirection );

                float3 viewDirection = normalize( _WorldSpaceCameraPos.xyz - i.posWorld.xyz );
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                
				#ifdef USING_DIRECTIONAL_LIGHT
					float3 lightDirection = _WorldSpaceLightPos0.xyz;
				#else
					//float3 lightDirection = _WorldSpaceLightPos0.xyz - posWorld.xyz * _WorldSpaceLightPos0.w;
					//lightDirection = NormalizePerVertexNormal(lightDir);
					//float3 lightDirection = normalize( _WorldSpaceLightPos0.xyz );
				#endif
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize( viewDirection + lightDirection );
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = _LightColor0.xyz * attenuation;
/////// GI Data:
			#ifdef SUPPORT_GI
                UnityLight light;
                #ifdef LIGHTMAP_OFF
                    light.color = lightColor;
                    light.dir = lightDirection;
                    light.ndotl = LambertTerm (normalDirection, light.dir);
                #else
                    light.color = half3(0.f, 0.f, 0.f);
                    light.ndotl = 0.0f;
                    light.dir = half3(0.f, 0.f, 0.f);
                #endif
                UnityGIInput d;
                d.light = light;
                d.worldPos = i.posWorld.xyz;
                d.worldViewDir = viewDirection;
                d.atten = attenuation;
                #if defined(LIGHTMAP_ON) || defined(DYNAMICLIGHTMAP_ON)
                    d.ambient = 0;
                    d.lightmapUV = i.ambientOrLightmapUV;
                #else
                    d.ambient = i.ambientOrLightmapUV;
                #endif
                Unity_GlossyEnvironmentData ugls_en_data;
                ugls_en_data.roughness = 1.0;
                ugls_en_data.reflUVW = viewReflectDirection;
                UnityGI gi = UnityGlobalIllumination(d, 1, normalDirection, ugls_en_data );
                lightDirection = gi.light.dir;
                lightColor = gi.light.color;
			#endif
///////// Gloss:
                float gloss = _Gloss;
                float specPow = exp2( gloss * 10.0 + 1.0 );
////// Specular:
                float3 specularColor = _SpecularColor.rgb * _Diffuse_var.a;
                float3 directSpecular = pow( max(0, dot( halfDirection, normalDirection ) ), specPow) * specularColor;
                float3 specular = directSpecular;
/////// Diffuse:
				float NdotL = dot( normalDirection, lightDirection );
                NdotL = lerp( NdotL, NdotL * 0.5 + 0.5, _LightWrap );	// Apply half-lambert
				NdotL = max( 0, NdotL);
                float3 directDiffuse = NdotL * attenColor;

                float3 indirectDiffuse = float3(0,0,0);
			#ifdef SUPPORT_GI
				indirectDiffuse += gi.indirect.diffuse;
			#endif
                
				float3 diffuseColor = (_Diffuse_var.rgb * _Color.rgb);
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular + _AmbientColor.rgb;
                fixed4 finalRGBA = fixed4(finalColor, 1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            //#pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            //#pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            //#pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            //#pragma multi_compile_fog
            //#pragma exclude_renderers d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _Diffuse; uniform float4 _Diffuse_ST;
            uniform float _AlphaCutoff;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                //float2 texcoord1 : TEXCOORD1;
                //float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
				o.uv0 = TRANSFORM_TEX(v.texcoord0, _Diffuse);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float4 _Diffuse_var = tex2D(_Diffuse, i.uv0);
                clip(_Diffuse_var.a - _AlphaCutoff);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
        Pass {
            Name "Meta"
            Tags {
                "LightMode"="Meta"
            }
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_META 1
            #include "UnityCG.cginc"
            #include "UnityMetaPass.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            //#pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            //#pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            //#pragma multi_compile_fog
            //#pragma exclude_renderers d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _Color;
            uniform sampler2D _Diffuse; uniform float4 _Diffuse_ST;
            uniform float4 _SpecularColor;
			uniform float4 _AmbientColor;
            uniform float _Gloss;
			uniform float _LightWrap;
			uniform float _AlphaCutoff;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD3;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
				o.uv0 = TRANSFORM_TEX(v.texcoord0, _Diffuse);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = UnityMetaVertexPosition(v.vertex, v.texcoord1.xy, v.texcoord2.xy, unity_LightmapST, unity_DynamicLightmapST );
                return o;
            }
            float4 frag(VertexOutput i) : SV_Target {
                UnityMetaInput o;
                UNITY_INITIALIZE_OUTPUT( UnityMetaInput, o );
                
                o.Emission = 0;
                
                float4 _Diffuse_var = tex2D(_Diffuse, i.uv0);
				clip(_Diffuse_var.a - _AlphaCutoff);

                float3 diffColor = (_Diffuse_var.rgb * _Color.rgb);
                float3 specColor = _SpecularColor.rgb * _Diffuse_var.a + _AmbientColor.rgb;
                float roughness = 1.0 - _Gloss;
                o.Albedo = diffColor + specColor * roughness * roughness * 0.5;
                
                return UnityMetaFragment( o );
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
}
