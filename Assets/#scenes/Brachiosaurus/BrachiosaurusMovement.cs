﻿using UnityEngine;
using System.Collections;

public class BrachiosaurusMovement : MonoBehaviour {
    Vector3 previous = new Vector3();
    // Use this for initialization
    void Start () {
        CreateSequence();
	}

    // Update is called once per frame
    void Update()
    {
        Vector3 vel = (transform.position - previous) / Time.deltaTime;

        previous = transform.position;

        if (vel != new Vector3(0, 0, 0))
            transform.rotation = Quaternion.LookRotation(vel);


    }
    void CreateSequence()
    {
        iTween.MoveTo(gameObject, iTween.Hash("path", iTweenPath.GetPath("BrachPath"), "time", 60f, "easeType", iTween.EaseType.easeInOutCubic));


    }
}