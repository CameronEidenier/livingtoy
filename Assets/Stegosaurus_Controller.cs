﻿using UnityEngine;
using System.Collections;

public class Stegosaurus_Controller : MonoBehaviour {
    Animator anim;
    public GameObject targetTRex;
    public GameObject targetBox;
    public bool tRex_A;
    Vector3 rotationLast;
    SkinnedMeshRenderer render;
    public bool active;
    float speed = .75f;
    Vector3 rotationPre;
    Quaternion originalRotation;
    Quaternion newLocalRotation;
    Quaternion newWorldRotation;
    float prevAngle = 0;
    bool turning = false;
    bool hurting = false;
    // Use this for initialization
    void Start () {
        render = GetComponentInChildren<SkinnedMeshRenderer>();
        anim = GetComponent<Animator>();

    }

    float angleChange;
    Quaternion lastQuat = new Quaternion();
    // Update is called once per frame
    void Update () {

        if (render.enabled) active = true;
        else active = false;

        if (targetTRex.transform.GetComponent<TRex_Controller>().attacking) hurting = true;
        else hurting = false;
        print(hurting);
        anim.SetBool("Hurt", hurting);
    }
    public float angleBetween2CartesianPoints(float firstX, float firstY, float secondX, float secondY)
    {
        float angle = Mathf.Atan2((secondX - firstX), (secondY - firstY)) * 180 / Mathf.PI;
        if (angle < 0)
        {
            return (360 + angle);
        }
        else
        {
            return (angle);
        }
    }
}
