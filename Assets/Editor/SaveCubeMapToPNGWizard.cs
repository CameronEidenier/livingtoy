﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;
using UnityEditor;
using System.Linq;

public class SaveCubeMapToPNGWizard : ScriptableWizard {

	Cubemap cubemap;
	public void OnWizardUpdate()
	{
		helpString = "Select cubemap to save to individual .png";
		if( Selection.activeObject is Cubemap && cubemap == null )
			cubemap = Selection.activeObject as Cubemap;

		isValid = ( cubemap != null );
	}
	
	// Update is called once per frame
	void OnWizardCreate () 
	{
		int width = cubemap.width;
		int height = cubemap.height;
		string savepath = Application.dataPath + "/" + cubemap.name;
		Debug.Log(savepath + "_PositiveX.png");
		Texture2D tex = new Texture2D(width, height, TextureFormat.RGB24, false);
		byte[] bytes = null;


//		//read screen contents into the texture
//		tex.SetPixels(cubemap.GetPixels(CubemapFace.PositiveX));
//		//encode texture into png
//		byte[] bytes = tex.EncodeToPNG();
//		File.WriteAllBytes(savepath + "_PositiveX.png", bytes );
//
//		tex.SetPixels(cubemap.GetPixels(CubemapFace.NegativeX));
//		bytes = tex.EncodeToPNG();
//		File.WriteAllBytes(savepath + "_NegativeX.png", bytes );
//
//		tex.SetPixels(cubemap.GetPixels(CubemapFace.PositiveY));
//		bytes = tex.EncodeToPNG();
//		File.WriteAllBytes(savepath + "_PositiveY.png", bytes );
//
//		tex.SetPixels(cubemap.GetPixels(CubemapFace.NegativeY));
//		bytes = tex.EncodeToPNG();
//		File.WriteAllBytes(savepath + "_NegativeY.png", bytes );
//
//		tex.SetPixels(cubemap.GetPixels(CubemapFace.PositiveZ));
//		bytes = tex.EncodeToPNG();
//		File.WriteAllBytes(savepath + "_PositiveZ.png", bytes );
//
//		tex.SetPixels(cubemap.GetPixels(CubemapFace.NegativeZ));
//		bytes = tex.EncodeToPNG();
//		File.WriteAllBytes(savepath + "_NegativeZ.png", bytes );


		// Iterate through 6 faces.
         for( int i = 0; i < 6; ++i )
         {
             // Encode texture into PNG.
             tex.SetPixels( cubemap.GetPixels( (CubemapFace)i ) );
 
             // Flip pixels on both axis (they are rotated for some reason).
             FlipPixels( tex, true, true );
 
             // Save as PNG.
             File.WriteAllBytes( Application.dataPath + "/" + cubemap.name + "_" + ( (CubemapFace)i ).ToString() + ".png", tex.EncodeToPNG() );
         }


		//we probably dont need to do this, garbage collection should grab it
		//DestroyImmediate(bytes);

	}

	public static void FlipPixels( Texture2D texture, bool flipX, bool flipY )
     {
         Color32[] originalPixels = texture.GetPixels32();
 
         var flippedPixels = Enumerable.Range( 0, texture.width * texture.height ).Select( index =>
             {
                 int x = index % texture.width;
                 int y = index / texture.width;
                 if( flipX )
                     x = texture.width - 1 - x;
 
                 if( flipY )
                     y = texture.height - 1 - y;
 
                 return originalPixels[y * texture.width + x];
             }
         );
 
         texture.SetPixels32( flippedPixels.ToArray() );
         texture.Apply();
     }

     [MenuItem( "GameObject/Save CubeMap To Png" )]
     static void SaveCubeMapToPng()
     {
		ScriptableWizard.DisplayWizard<SaveCubeMapToPNGWizard>( "Save CubeMap To Png", "Save" );
     }

}
